$(function(){
$("#elastic_grid_demo").elastic_grid({	
	'hoverDirection': true,
	'hoverDelay': 0,
	'hoverInverse': false,
	'expandingSpeed': 500,
	'expandingHeight': 500,
	'items' :
		[
			{
			'title' : 'Soporte Remoto y Onsite ',
			'description'   : 'Servicio de Soporte y Onsite',
			'thumbnail' : ['img/01_SOPORTE/servicio1.jpg', 'img/portfolio/small/01_SOPORTE/servicio1-1.jpg'],
			'large' : ['img/portfolio/large/01_SOPORTE/servicio1.jpg', 'img/portfolio/large/01_SOPORTE/servicio1-1.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Servicios', 'Todo']
			},

			{
			'title' : 'Instalación y Despliegue',
			'description'   : 'Servicio de instalación y despliegue',
			'thumbnail' : ['img/02_INSTALACION/servicio2.jpg', 'img/portfolio/small/02_INSTALACION/servicio2-1.jpg', 'img/portfolio/small/02_INSTALACION/servicio2-2.jpg', 'img/portfolio/small/02_INSTALACION/servicio2-3.jpg','img/portfolio/small/02_INSTALACION/servicio2-4.jpg','img/portfolio/small/02_INSTALACION/servicio2-5.jpg','img/portfolio/small/02_INSTALACION/servicio2-6.jpg','img/portfolio/small/02_INSTALACION/servicio2-7.jpg','img/portfolio/small/02_INSTALACION/servicio2-8.jpg','img/portfolio/small/02_INSTALACION/servicio2-9.jpg'],
			'large' : ['img/portfolio/large/02_INSTALACION/servicio2.jpg', 'img/portfolio/large/02_INSTALACION/servicio2-1.jpg', 'img/portfolio/large/02_INSTALACION/servicio2-2.jpg', 'img/portfolio/large/02_INSTALACION/servicio2-3.jpg','img/portfolio/large/02_INSTALACION/servicio2-4.jpg','img/portfolio/large/02_INSTALACION/servicio2-5.jpg','img/portfolio/large/02_INSTALACION/servicio2-6.jpg','img/portfolio/large/02_INSTALACION/servicio2-7.jpg','img/portfolio/large/02_INSTALACION/servicio2-8.jpg','img/portfolio/large/02_INSTALACION/servicio2-9.jpg'],
			'button_list'   :
			[
            { 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Servicios', 'Todo']
			},
            {
			'title' : 'Gestión de Proyectos',
			'description'   : 'Servicio de Gestión de Proyectos.',
			'thumbnail' : ['img/03_PROYECTOS/servicio3.jpg', 'img/portfolio/small/03_PROYECTOS/servicio3-1.jpg', 'img/portfolio/small/03_PROYECTOS/servicio3-2.jpg'],
			'large' : ['img/portfolio/large/03_PROYECTOS/servicio3.jpg', 'img/portfolio/large/03_PROYECTOS/servicio3-1.jpg', 'img/portfolio/large/03_PROYECTOS/servicio3-2.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Servicios', 'Todo']
			},
             {
			'title' : 'Gestión de Insfraestructura TI',
			'description'   : 'Servicio de Gestion de Infraestructura TI.',
			'thumbnail' : ['img/04_GESTION_INFRA/servicio4.jpg', 'img/portfolio/small/04_GESTION_INFRA/servicio4-1.jpg', 'img/portfolio/small/04_GESTION_INFRA/servicio4-2.jpg', 'img/portfolio/small/04_GESTION_INFRA/servicio4-3.jpg'],
			'large' : ['img/portfolio/large/04_GESTION_INFRA/servicio4.jpg', 'img/portfolio/large/04_GESTION_INFRA/servicio4-1.jpg', 'img/portfolio/large/04_GESTION_INFRA/servicio4-2.jpg', 'img/portfolio/large/04_GESTION_INFRA/servicio4-3.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Servicios', 'Todo']
			},
            {
			'title' : 'Consultoría, Auditoria y Capacitación',
			'description'   : 'Servicio de Consultoría, Auditoria y Capacitación .',
			'thumbnail' : ['img/05_CONSULTORIA/servicio5.jpg', 'img/portfolio/small/05_CONSULTORIA/servicio5-1.jpg', 'img/portfolio/small/05_CONSULTORIA/servicio5-2.jpg'],
			'large' : ['img/portfolio/large/05_CONSULTORIA/servicio5.jpg', 'img/portfolio/large/05_CONSULTORIA/servicio5-1.jpg', 'img/portfolio/large/05_CONSULTORIA/servicio5-2.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Servicios', 'Todo']
			},
 {
			'title' : 'Equipamento y Licenciamiento',
			'description'   : 'Servicio de Equipamento y Licenciamiento',
			'thumbnail' : ['img/06_EQUIPAMIENTO/servicio6.jpg', 'img/portfolio/small/06_EQUIPAMIENTO/servicio6-1.jpg','img/portfolio/small/06_EQUIPAMIENTO/servicio6-3.jpg','img/portfolio/small/06_EQUIPAMIENTO/servicio6-4.jpg', 'img/portfolio/small/06_EQUIPAMIENTO/servicio6-5.jpg','img/portfolio/small/06_EQUIPAMIENTO/servicio6-6.jpg','img/portfolio/small/06_EQUIPAMIENTO/servicio6-7.jpg'],
			'large' : ['img/portfolio/large/06_EQUIPAMIENTO/servicio6.jpg', 'img/portfolio/large/06_EQUIPAMIENTO/servicio6-1.jpg', 'img/portfolio/large/06_EQUIPAMIENTO/servicio6-3.jpg','img/portfolio/large/06_EQUIPAMIENTO/servicio6-4.jpg','img/portfolio/large/06_EQUIPAMIENTO/servicio6-5.jpg','img/portfolio/large/06_EQUIPAMIENTO/servicio6-6.jpg','img/portfolio/large/06_EQUIPAMIENTO/servicio6-7.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Servicios', 'Todo']
			},
			{
			'title' : 'Arrendamiento Operativo y Financiamiento',
			'description'   : 'Soluciones de Arrendamiento Operativo y Financiamiento',
			'thumbnail' : ['img/solucion1.jpg','img/portfolio/small/solucion-2.jpg', 'img/portfolio/small/solucion-3.jpg', 'img/portfolio/small/solucion-4.jpg'],
			'large' : ['img/portfolio/large/solucion-1.jpg','img/portfolio/large/solucion-2.jpg', 'img/portfolio/large/solucion-3.jpg', 'img/portfolio/large/solucion-4.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
            {
			'title' : 'Redes y Comunicaciones',
			'description'   : 'Soluciones de Redes y Comunicaciones',
			'thumbnail' : ['img/solucion2.jpg','img/portfolio/small/solucion-3.jpg', 'img/portfolio/small/solucion-4.jpg', 'img/portfolio/small/solucion-5.jpg'],
			'large' : ['img/portfolio/large/solucion-2.jpg','img/portfolio/large/solucion-3.jpg', 'img/portfolio/large/solucion-4.jpg', 'img/portfolio/large/solucion-5.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
              {
			'title' : 'Cloud Computing',
			'description'   : 'Soluciones con Cloud Computing.',
			'thumbnail' : ['img/solucion3.jpg','img/portfolio/small/solucion-4.jpg', 'img/portfolio/small/solucion-5.jpg', 'img/portfolio/small/solucion-6.jpg'],
			'large' : ['img/portfolio/large/solucion-3.jpg','img/portfolio/large/solucion-4.jpg', 'img/portfolio/large/solucion-5.jpg', 'img/portfolio/large/solucion-6.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
              {
			'title' : 'Almacenamiento y Respaldo',
			'description'   : 'Soluciones de Almacenamiento y Respaldo',
			'thumbnail' : ['img/solucion4.jpg','img/portfolio/small/solucion-5.jpg', 'img/portfolio/small/solucion-6.jpg', 'img/portfolio/small/solucion-7.jpg'],
			'large' : ['img/portfolio/large/solucion-4.jpg','img/portfolio/large/solucion-5.jpg', 'img/portfolio/large/solucion-6.jpg', 'img/portfolio/large/solucion-7.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
              {
			'title' : 'Correo y Colaboración',
			'description'   : 'Soluciones de Correo y Colaboración',
			'thumbnail' : ['img/solucion5.jpg','img/portfolio/small/solucion-6.jpg', 'img/portfolio/small/solucion-7.jpg', 'img/portfolio/small/solucion-8.jpg'],
			'large' : ['img/portfolio/large/solucion-5.jpg','img/portfolio/large/solucion-6.jpg', 'img/portfolio/large/solucion-7.jpg', 'img/portfolio/large/solucion-8.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
              {
			'title' : 'Virtualización de Servidores y Desktop',
			'description'   : 'Soluciones de Virtualización de Servidores y Desktop',
			'thumbnail' : ['img/solucion6.jpg','img/portfolio/small/solucion-7.jpg', 'img/portfolio/small/solucion-8.jpg', 'img/portfolio/small/solucion-9.jpg'],
			'large' : ['img/portfolio/large/solucion-6.jpg','img/portfolio/large/solucion-7.jpg', 'img/portfolio/large/solucion-8.jpg', 'img/portfolio/large/solucion-9.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
              {
			'title' : 'Seguridad Perimental',
			'description'   : 'Soluciones de Seguridad Perimental',
			'thumbnail' : ['img/solucion7.jpg','img/portfolio/small/solucion-8.jpg', 'img/portfolio/small/solucion-9.jpg', 'img/portfolio/small/solucion-10.jpg'],
			'large' : ['img/portfolio/large/solucion-7.jpg','img/portfolio/large/solucion-8.jpg', 'img/portfolio/large/solucion-9.jpg', 'img/portfolio/large/solucion-10.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
              {
			'title' : 'Alta Disponibilidad',
			'description'   : 'Soluciones con Alta Disponibilidad.',
			'thumbnail' : ['img/solucion8.jpg','img/portfolio/small/solucion-9.jpg', 'img/portfolio/small/solucion-10.jpg', 'img/portfolio/small/solucion-1.jpg'],
			'large' : ['img/portfolio/large/solucion-8.jpg','img/portfolio/large/soluciones-9.jpg', 'img/portfolio/large/soluciones-10.jpg', 'img/portfolio/large/solucion-1.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
              {
			'title' : 'Protección eléctrica',
			'description'   : 'Soluciones de Protección eléctrica.',
			'thumbnail' : ['img/solucion9.jpg','img/portfolio/small/solucion-10.jpg', 'img/portfolio/small/solucion-1.jpg', 'img/portfolio/small/solucion-2.jpg'],
			'large' : ['img/portfolio/large/solucion-9.jpg','img/portfolio/large/solucion-10.jpg', 'img/portfolio/large/solucion-1.jpg', 'img/portfolio/large/solucion-2.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			},
            
            {
			'title' : 'Seguridad Electrónica',
			'description'   : 'Soluciones de Seguridad Electrónica',
			'thumbnail' : ['img/solucion10.jpg','img/portfolio/small/solucion-1.jpg', 'img/portfolio/small/solucion-2.jpg', 'img/portfolio/small/solucion-3.jpg'],
			'large' : ['img/portfolio/large/solucion-10.jpg','img/portfolio/large/solucion-1.jpg', 'img/portfolio/large/solucion-2.jpg', 'img/portfolio/large/solucion-3.jpg'],
			'button_list'   :
			[
			{ 'title':'Contenido', 'url' : 'http://#' },
			{ 'title':'Descarga', 'url':'http://#'}
			],
			'tags'  : ['Soluciones', 'Todo']
			}

			
		]
	});
});